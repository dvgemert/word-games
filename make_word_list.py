import argparse
import random
import sys

def filter_words_of_length(input, length: int, n: int = None, sample_first = False) -> list[str]:
	"""
	Opens a text file (assumed to be a dictionary) and returns a list containing
	only words with `length` letters. This list contains at most `n` words,
	randomly sampled, or all words from the input if `None`.

	Args:
		filename: The path to the text file.
		length: The length of the words in the output.
		n: The maximum number of words to output.

	Returns:
		A list of `n` words with `length` letters from the file.
	"""
	all_words_of_length = []
	for line in input:
		word = line.strip() # Remove leading/trailing whitespaces
		if len(word) == length:
			all_words_of_length.append(word)
	
	if n is None:
		return sorted(all_words_of_length)
	elif sample_first:
		n = min(n, len(all_words_of_length))
		return sorted(all_words_of_length[:n])
	else:
		n = min(n, len(all_words_of_length))
		return sorted(random.sample(all_words_of_length, n))

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("length", type=int, help="length of the words.")
	parser.add_argument("-n", type=int, help="number of words in the output, randomly sampled. Defaults to all words.")
	parser.add_argument("--sample", choices=["random", "first"], help="how to sample words.\nrandom: sample randomly.\nfirst: take the first n words from the list (deterministic).\nDefaults to sampling randomly.")
	parser.add_argument("-i", "--input", type=argparse.FileType("r"), default=sys.stdin, help="input dictionary file. Defaults to reading from stdin.")
	parser.add_argument("-o", "--output", type=argparse.FileType("w"), default=sys.stdout, help="output file. Defaults to writing to stdout.")
	args = parser.parse_args()
	
	words = filter_words_of_length(args.input, args.length, args.n, args.sample == "first")
	
	for word in words:
		args.output.write(word + "\n") # Add newline character for each word
	
	if args.n is not None and len(words) != args.n:
		print(f"Warning: not enough {args.length}-letter words in the input, output contains only {len(words)} words.", file=sys.stderr)
	
	if args.input != sys.stdin:
		args.input.close()
	if args.output != sys.stdout:
		args.output.close()

if __name__ == "__main__":
	main()
