from word_peaks import *
from datetime import datetime

def get_dictionary():
    return make_kdtree(WordPeaks.read_words("./dictionaries/word_peaks_targets.txt"))

def test_word_distance():
    assert word_distance("hello", "hello") == 0
    assert word_distance("hello", "hewwo") == 22

def test_closest_word():
    dictionary = get_dictionary()
    closest = closest_word("hewwo", dictionary)
    assert closest == "metro"

def bench(f, n):
    dictionary = get_dictionary()
    start_time = datetime.now()
    for _ in range(n):
        f(rand_word(5), dictionary)
    end_time = datetime.now()
    print(end_time - start_time)

def test_word_neighbours():
    dictionary = get_dictionary()
    assert word_neighbours("hello", dictionary) == set(["hello"])
    assert word_neighbours("eight", dictionary) == set(["eight", "fight", "digit"])

test_word_distance()
test_closest_word()
test_word_neighbours()

bench(closest_word, 10000)
bench(word_neighbours, 10000)
