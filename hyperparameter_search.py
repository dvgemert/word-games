import multiprocessing
import itertools
import numpy as np
import word_peaks
from tqdm import tqdm
from os.path import isfile

N_ITERATIONS_THIS_SCRIPT = 250 # The iterations this script will execute

def run(args):
    max_guesses = args[10]
    all_guesses = np.zeros(N_ITERATIONS_THIS_SCRIPT)
    #all_diversities = np.full((n_iterations, max_guesses), np.NaN)

    filename = f"./results/hyperparam_search/elitist {args[1]} memetic {args[2]} k {args[4]} mu {args[5]} max_guesses {max_guesses} selection {args[0].__name__}.npy"
    if isfile(filename):
        print(f"'{filename}' is already a file; skipping this run")
        return
    for run_i in range(N_ITERATIONS_THIS_SCRIPT):
        guesses, diversities = word_peaks.play_game(*args)
        all_guesses[run_i] = guesses

    np.save(filename, all_guesses)

def main():
    param_dict = {
    "parent_selection_function": [word_peaks.GA.probabilistic_selection, word_peaks.GA.tournament_selection],
    "elitist" : [False, True],
    "memetic": [False, True],
    "population_size": [200],
    "k": [2, 3, 4, 5],
    "mu": [0, 0.001, 0.01, 0.08, 0.1, 0.15],
    "do_print": [False],
    "use_bs": [False],
    "measure_diversity": [False],
    "n_iterations": [1], # The n_iterations to pass to play_game
    "max_guesses": [50],
    }

    vals = param_dict.values()
    combinations = list(itertools.product(*vals))
    pool = multiprocessing.Pool()
    for _ in tqdm(pool.imap_unordered(run, combinations), total=len(combinations)):
        pass
    
if __name__ == "__main__":
    main()