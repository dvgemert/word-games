from pathlib import Path
from random import randrange, randint
import random
import math
import numpy as np
from scipy.spatial import KDTree
from itertools import combinations

LOWER_HINT_STR = 'v'
HIGHER_HINT_STR = '^'
EQUAL_HINT_STR = 'X'

class WordPeaks():
    """Object for a game of Word Peaks"""

    def __init__(self, dictionary_path = Path("dictionary.txt"), targets_path = Path("dictionaries/word_peaks_targets.txt"), use_targets_as_dictionary = True, max_guesses = 6, inf_guesses = True) -> None:
        """Initialize a game of Word Peaks"""

        self.targets = self.read_words(targets_path)
        self.targets_kd = make_kdtree(self.targets)
        if use_targets_as_dictionary:
            self.dictionary = self.targets
            self.dictionary_kd = self.targets_kd
        else:
            self.dictionary = self.read_words(dictionary_path)
            self.dictionary_kd = make_kdtree(self.dictionary)
        self.target = self.targets[randrange(len(self.targets))]
        self.inf_guesses = inf_guesses
        self.remaining_guesses = max_guesses
        self.word_len = len(self.dictionary[0])

    def guess(self, guess: str) -> str:
        """Guess a word\nReturns for each letter whether it is too high or low"""
        if not self.inf_guesses and self.remaining_guesses == 0:
            return ""
        if guess not in self.dictionary:
            raise ValueError("Word is not in the dictionary")
        self.remaining_guesses -= 1
        return self.get_hints(guess)

    def get_hints(self, guess: str) -> str:
        """Compares `guess` and `target` strings.\nReturns for each character whether it should be lower or higher"""
        s = ""
        for g, t in zip(guess, self.target):
            if g < t:
                s += HIGHER_HINT_STR
            if g == t:
                s += EQUAL_HINT_STR
            if g > t:
                s += LOWER_HINT_STR
        return(s)

    def make_valid_word(self, word: str) -> str:
        """Turn a string into the closest valid word from the dictionary"""
        return closest_word(word, self.dictionary_kd)

    @staticmethod
    def read_words(path: Path) -> list[str]:
        """Reads all lines of a text file into a list\n
        Returns: `words` the list of words"""
        with open(path, 'r') as f:
            words = f.read().splitlines()
        return words
    
class GA():
    """Genetic Algorithm class"""
    def __init__(self, wp: WordPeaks, parent_selection_function, elitist = False, memetic = False, population_size = 100, k = 3, mu = 0.08, do_print = True, measure_diversity = False) -> None:
        if population_size % 2 != 0: raise Exception("n must be a multiple of 2")
        self.population_size = population_size
        self.wp = wp
        self.ord_ranges = [[ord('a'), ord('z')] for _ in range(wp.word_len)]
        self.population = [rand_word(wp.word_len) for _ in range(population_size)]
        self.population = [wp.make_valid_word(word) for word in self.population]
        self.k = k
        self.mu = mu
        self.guesses = []
        self.do_print = do_print
        self.parent_selection_function = parent_selection_function
        self.elitist = elitist
        self.memetic = memetic
        self.measure_diversity = measure_diversity
        if measure_diversity:
            self.diversities = np.full(wp.remaining_guesses, np.NaN)
            self.guess_index = 0
        else:
            self.diversities = None

    def __str__(self) -> str:
        return str(self.population)
    
    def log(self, msg):
        """Print if printing is enabled"""
        if self.do_print: print(msg)
    
    def iteration(self):
        """Compute the next generation and guess the fittest individual\n
        Returns:\n
        `hints`, the hints for each letter returned by the WordPeaks instance"""
        # Make new generation
        self.make_generation()
        # Guess fittest
        guess = max(self.population, key=self.fitness)
        fitness = self.fitness(guess)
        debug_fitness = self.debug_optimal_fitness(guess)
        self.guesses.append((guess, fitness, debug_fitness))
        hints = self.wp.guess(guess)
        if hints == "": return None # max guesses reached
        self.update_ranges(guess, hints)
        self.log_diversity()
        self.log(f"{guess} {hints} {fitness:4.0f} {debug_fitness:3.0f} {sorted(self.population, key=self.fitness, reverse=True)[:5]}")
        return hints
    
    def run(self) -> tuple[str|None, int, np.ndarray]:
        """Run the GA\n
        Returns:\n
        `target`: The target word
        `i`: Guesses until success OR `None` for failure
        `diversities`: Population diversities per generation"""
        self.log(self.wp.target)
        hints = ""
        i = 0
        while hints != EQUAL_HINT_STR*self.wp.word_len:
            hints = self.iteration()
            i += 1
            if hints is None: return (None, i, self.diversities) # max guesses reached
        self.log(f'Word "{self.wp.target}" guessed in {i} generations')
        return (self.wp.target, i, self.diversities)
        
    def make_generation(self) -> None:
        """Produce new generation based on the chosen parent selection function"""
        new_population = self.population.copy()
        for i in range(0, self.population_size, 2):
            parents = self.parent_selection_function(self)
            children = crossover(parents[0], parents[1])
            children = [self.mutate_uniform(child, self.mu) for child in children]
            if self.memetic:
                children = [self.local_search(child) for child in children]
            # Replace parents by their children in the new generation
            for j in range(2):
                if self.fitness(children[j]) >= self.fitness(parents[j]) or not self.elitist: 
                    new_population[i+j] = children[j]
        self.population = new_population

    def tournament_selection(self):
        """Select parents by tournament selection"""
        parents = ["", ""]
        parent_i = 0
        # Make children
        while parent_i < 2:
            # Select K random specimen
            np.random.shuffle(self.population)
            tournament = self.population[:self.k]
            # Select fittest
            parent = max(tournament, key=self.fitness)
            # Prevent same parent from being selected twice
            if parent not in parents:
                parents[parent_i] = parent
                parent_i += 1
        return parents

    def probabilistic_selection(self):
        """Select parents by probability per individual based on fitness"""
        fitnesses = np.array([self.fitness(specimen) for specimen in self.population])
        probabilities = fitnesses / fitnesses.sum()
        return np.random.choice(self.population, 2, p=probabilities)
        
    
    def local_search(self, word: str) -> str:
        """Perform local search to `word`, returns the neighbouring word with the highest fitness. To be used for memetic algorithm."""
        return max(word_neighbours(word, self.wp.dictionary_kd), key=self.fitness)
        
    def update_ranges(self, guess: str, hints: str) -> None:
        """Update the internal ranges of each letter based on the `hints` that were given for the `guess`"""
        for letter_idx in range(self.wp.word_len):
            if hints[letter_idx] == EQUAL_HINT_STR:
                self.ord_ranges[letter_idx] = [ord(guess[letter_idx]), ord(guess[letter_idx])]
            if hints[letter_idx] == HIGHER_HINT_STR:
                self.ord_ranges[letter_idx][0] = ord(guess[letter_idx]) + 1
            if hints[letter_idx] == LOWER_HINT_STR:
                self.ord_ranges[letter_idx][1] = ord(guess[letter_idx]) - 1

    def mutate_uniform(self, word: str, mutate_prob) -> str:
        """With a uniform `mutate_prob`, replace each letter by a random letter. Return the closest word in the dictionary to the resulting mutated string."""
        new_word = ""
        for letter, ord_range in zip(word, self.ord_ranges):
            if random.random() > mutate_prob:
                new_word += letter
            else:
                new_word += chr(random.choice(range(ord_range[0], ord_range[1]+1)))
        return self.wp.make_valid_word(new_word)
    
    def debug_optimal_fitness(self, word: str) -> float:
        """Theortical optimal fitness function derived from the distance of the word to the currnt game's target word"""
        return -word_distance(word, self.wp.target)
    
    def fitness(self, word: str) -> float:
        """Fitness function derived from the distance of each letter from the middle of the acceptable letter range obtained from the game hints thus far"""
        fitness = 0
        for c in range(self.wp.word_len):
            if ord(word[c]) < self.ord_ranges[c][0] or ord(word[c]) > self.ord_ranges[c][1]:
                fitness -= 100
                continue
            middle_point = (self.ord_ranges[c][0] + self.ord_ranges[c][1]) // 2
            dist = abs(ord(word[c]) - middle_point)
            fitness -= dist
        return fitness
    
    def diversity(self) -> float:
        word_pairs = list(combinations(self.population, 2))
        diversity = 0
        for word_pair in word_pairs:
            diversity += word_distance(word_pair[0], word_pair[1])
        return diversity / len(word_pairs)
    
    def log_diversity(self):
        if self.measure_diversity:
            self.diversities[self.guess_index] = self.diversity()
            self.guess_index += 1

class BinarySearcher():
    def __init__(self, wp_game: WordPeaks, do_print = True) -> None:
        self.wp = wp_game
        self.ord_ranges = [[ord('a'), ord('z')] for _ in range(self.wp.word_len)]
        self.guesses = []
        self.do_print = do_print
    
    def log(self, msg):
        if self.do_print: print(msg)
    
    def run(self):
        self.log(self.wp.target)
        i = 0
        hints = ""
        while hints != EQUAL_HINT_STR*self.wp.word_len:
            i += 1
            hints = self.iteration()
            if hints is None: return (None, i, None) # max guesses reached
        self.log(f'Word "{self.wp.target}" guessed in {i} generations')
        return (self.wp.target, i, None)
    
    def iteration(self):
        guess = self.wp.make_valid_word(mid_word(self.ord_ranges))
        fitness = GA.fitness(self, guess) # type: ignore
        debug_fitness = GA.debug_optimal_fitness(self, guess) # type: ignore
        self.guesses.append((guess, fitness, debug_fitness))
        hints = self.wp.guess(guess)
        if hints == "": return None # max guesses reached
        GA.update_ranges(self, guess, hints) # type: ignore
        self.log(f"{guess} {hints} {fitness:4.0f} {debug_fitness:3.0f}")
        return hints

def rand_word(word_len) -> str:
    """Returns random word of length `word_len`"""
    return "".join([rand_char() for _ in range(word_len)])

def rand_char() -> str:
    """Returns random lowercase character between 'a' and 'z'"""
    return chr(randint(ord('a'), ord('z')))

def mid_word(ranges: list[list[int]]) -> str:
    """Returns a word with each letters in the middle of the currently known range"""
    return "".join([chr((high + low) // 2) for [high, low] in ranges])

def binary_search(ls, target) -> int:
    """Modified binary search on `ls`.\n
    Returns the index of `target`. If the target is not in the list, the closest index is returned."""
    left = 0
    right = len(ls) - 1
    while left <= right:
        mid = (left + right) // 2
        if ls[mid] == target:
            return mid
        elif ls[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return left

def word_distance(a: str, b: str) -> int:
    """Return the distance between two words, measured by the sum of the pairwise ordinal distances between each letter"""
    dist = 0
    for i in range(len(a)):
        dist += abs(ord(a[i]) - ord(b[i]))
    return dist

def closest_word(guess: str, dictionary: KDTree) -> str:
    """Return the closest word to `guess` that is in the `dictionary` KDTree"""
    # p=1 for manhattan distance
    (dist, nearest_idx) = dictionary.query([ord(c) for c in guess], p=1)
    return "".join([chr(int(c)) for c in dictionary.data[nearest_idx]])

def word_neighbours(word: str, dictionary: KDTree) -> set[str]:
    neighbours = dictionary.query_ball_point([ord(c) for c in word], 1, p=math.inf)
    return set("".join([chr(int(c)) for c in dictionary.data[i]]) for i in neighbours)

def make_kdtree(dictionary: list[str]) -> KDTree:
    """Make a KDTree out of a list of strings"""
    return KDTree([[ord(c) for c in x] for x in dictionary]) # type: ignore

def crossover(word1: str, word2: str) -> tuple[str, str]:
    """Perform 'gene crossover' on two words at a random splicing point"""
    slice = randint(1, len(word1)-1)
    return word1[slice:] + word2[:slice], word2[slice:] + word1[:slice]

def play_game(parent_selection_function = GA.tournament_selection, elitist = False, memetic = False, population_size=200, k=5, mu=0.15, do_print=False, use_bs = False, measure_diversity = False, n_iterations=1, max_guesses = 6,  seed=None, targets_path = Path("dictionaries/word_peaks_targets.txt"), inf_guesses=False) -> tuple[int, np.ndarray]:
    """Play word peaks\n
    Returns:
    `guesses`: The number of guesses until success or `max_guesses + 1` for a failure
    `diversities`: Population diversities per generation"""
    if seed:
        random.seed(seed)
        np.random.seed(seed)
    
    n_guessed = 0
    
    for i in range(n_iterations):
        wp = WordPeaks(inf_guesses=inf_guesses, max_guesses=max_guesses, targets_path=targets_path)

        if use_bs: ga = BinarySearcher(wp, do_print=do_print)
        else: ga = GA(wp, parent_selection_function, elitist=elitist, memetic=memetic, population_size=population_size, k=k, mu=mu, do_print=do_print, measure_diversity=measure_diversity)
        (word, guesses, diversities) = ga.run()
        if word is not None:
            n_guessed += 1
    if n_iterations == 1: return guesses, diversities
    return n_guessed, diversities

def main() -> None:
    n_iterations = 100
    
    ga_guessed_in_time = play_game(n_iterations=n_iterations, memetic=True)
    print(f"{ga_guessed_in_time} of {n_iterations} guessed in time ({ga_guessed_in_time / n_iterations * 100:.0f}%)")
    # print(f"{bs_guessed_in_time} of {n_iterations} guessed in time ({bs_guessed_in_time / n_iterations * 100:.0f}%)")
    
    # wp = WordPeaks(max_guesses=100, inf_guesses=False, targets_path=Path("dictionaries/dict-2.txt"))
    # wp.target = "so"
    # bs = BinarySearcher(wp, do_print=True)
    # print(bs.run())

if __name__ == '__main__':
    main()
