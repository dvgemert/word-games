# Genetic Algorithm Solver for [Word Peaks](https://vegeta897.github.io/word-peaks/)

## Usage
Use `pip install -r requirements.txt` to install required libraries. Code was tested with Miniconda on Python version 3.11.5. Standalone Python requires an ipykernel to run Jupyter Notebooks.

The code running our implementation of Word Peaks and the Genetic Algorithm (GA) can be found in `word_peaks.py`. `hyperparameter_search.py` is used to exhaustively run GAs with different parameters and `analyze_hyperparameter_search.ipynb` is used to evaluate these. `analysis.ipynb` makes the graphs to evaluate GAs running with fixed values or a subset for the parameters. `make_word_list.py` is used to extract from dictionary files; pass `--help` for usage information.


## Data sources:
- `dictionaries/words_alpha.txt`:
  
  URL: https://github.com/dwyl/english-words/blob/master/words_alpha.txt  
  License: [public domain (Unlicense)](https://github.com/dwyl/english-words/blob/master/LICENSE.md)

- `dictionaries/engmix.txt`, `dictionaries/english3.txt`:
  
  URL: http://gwicks.net/justwords.html  
  License: http://gwicks.net/downloads.htm

- `dictionaries/word_peaks_targets.txt`:
  
  URL: https://github.com/vegeta897/word-peaks/blob/main/src/lib/words/targets-filtered.json  
  License: [MIT](https://github.com/vegeta897/word-peaks/blob/main/LICENSE)

- `dictionaries/count_1w.txt`:
  
  URL: https://norvig.com/ngrams/count_1w.txt
  License: [MIT](https://norvig.com/ngrams/)
